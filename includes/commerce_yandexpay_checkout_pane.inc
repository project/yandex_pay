<?php

function commerce_yandexpay_btn_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  $pane_form = array();

  $pane_form['yandex_pay'] = array(
    '#theme' => 'yandex_pay_checkout_btn',
    '#cart' => null,
  );

  return $pane_form;
}

