<?php

/*
 *  Запрос информации для отображения корзины
 *  /v1/order/render
 *
 *  $token - после валидации содержит информацию о заказе
 *  
*/
function wrapperYandexOrderRender(){
  global $yp_commerce_currency;

  $content = trim(file_get_contents('php://input'));
  $YandexPayAPI = new YandexPayAPI(YP_SETTINGS_RAW);
  $token = $YandexPayAPI->JWTValidation($content);

  if(isset($token->api_message)){ // Валидация токена не успешна
    drupal_add_http_header('Status', $token->api_message->reason, TRUE);
    drupal_json_output($token->api_message);   
  }
  
  if(!isset($token->cart)){
    $messages = $YandexPayAPI->errorMessages(403, 'There is no basket in tokenet');
    drupal_add_http_header('Status', $messages->api_message->reason, TRUE);
    drupal_json_output($messages->api_message); 
  }
  
  $yp_commerce_currency = commerce_currency_load();

  /*
   * Настройки магазина
   *
  */
  $store_settings = [
    'currencyCode' => $yp_commerce_currency['code']
  ];
  
  /*
   * Информация о доставке
   *
  */  
  $shipping = null;
  
  /*
   * Информация о товарах в заказе
   *
  */
  $cart = ['cart_total' => 0];

  foreach($token->cart->items as $item){
    $product_id = $item->productId;
    $qty = $item->quantity->count;
   
    $product = commerce_product_load($product_id);

    if(is_object($product)){
      if(isset($product->commerce_price)){
        $product_price = $product->commerce_price[LANGUAGE_NONE][0]['amount'];
        $price = commerce_currency_amount_to_decimal($product_price, $yp_commerce_currency['code']);
        
        $calculate = commerce_product_calculate_sell_price($product);
        $sell_price = commerce_currency_amount_to_decimal($calculate['amount'], $yp_commerce_currency['code']);
        
        if($calculate['amount'] > $product_price){
          $price = $sell_price;
        }
        
        $cart['items'][] = [
          'productId' => $product->product_id,
          'unitPrice' => number_format($price, 2, '.', ''),
          'discountedUnitPrice' => number_format($sell_price, 2, '.', ''),
          'subtotal' => number_format($price * $qty, 2, '.', ''),
          'total' => number_format($sell_price * $qty, 2, '.', ''),
          'title' => $product->title,
          'qty_count' => number_format($qty, 2, '.', ''),       
        ];
        
        $cart['cart_total'] += $sell_price * $qty;
      }
    }  
  }
  
  // Заказ из нативной формы оформления заказа
  if($token->metadata == 'nativ_payment'){
    //$external_id = explode('_', $token->orderId);
    $external_id = explode('_', $token->cart->externalId);
    
    $shipping = [
      'title' => YP_SETTINGS_RAW['delivery_name'],
      'amount' => 0
    ];

    if(count($external_id) == 3){
      if(is_numeric($external_id[0])){
        $order = commerce_order_load($external_id[0]);
        
        if(is_object($order)){
          $order->status = 'pending';
          
          foreach($order->commerce_line_items[LANGUAGE_NONE] as $line_item){
            $item = commerce_line_item_load($line_item['line_item_id']);
            
            if($item->type == 'shipping'){
              $shipping = [
                'title' => $item->line_item_label,
                'amount' => commerce_currency_amount_to_decimal($item->commerce_total[LANGUAGE_NONE][0]['amount'], $yp_commerce_currency['code'])
              ];
              
              $cart['cart_total'] += $shipping['amount'];
            }
          }
        }  
      }
    }
  }
  
  $cart['cart_total'] = number_format($cart['cart_total'], 2, '.', '');
  //$cart['externalId'] = $token->orderId;
  $cart['externalId'] = $token->cart->externalId;

  $output = $YandexPayAPI->yandexOrderRender($store_settings, $cart, $shipping);

  drupal_json_output($output);    
}

/*
 *  Создание заказа
 *  /v1/order/create
 *
 *  $token - после валидации содержит информацию о заказе
 *  
*/
function wrapperYandexOrderCreate(){

  $content = trim(file_get_contents('php://input'));
  $YandexPayAPI = new YandexPayAPI(YP_SETTINGS_RAW);
  $token = $YandexPayAPI->JWTValidation($content);

  if(isset($token->api_message)){ // Валидация токена не успешна
    drupal_add_http_header('Status', $token->api_message->reason, TRUE);
    drupal_json_output($token->api_message);   
  } 

  $items            = $token->cart->items;  
  $shipping         = $token->shippingAddress;
  $shippingContact  = $token->shippingContact;
  $billingContact   = $token->billingContact;
  
  $thoroughfare = $shipping->street.' '.$shipping->building.', ';
  $thoroughfare .= t('room').' ';
  $thoroughfare .= $shipping->room;
  
  if(isset($billingContact->lastName) ){
    $billingContact->lastName = is_null($billingContact->lastName) ? $shippingContact->lastName : $billingContact->lastName;
  }else{
    $billingContact->lastName = $shippingContact->lastName;
  }
  
  if(isset($billingContact->firstName) ){
    $billingContact->firstName = is_null($billingContact->firstName) ? $shippingContact->firstName : $billingContact->firstName;
  }else{
    $billingContact->firstName = $shippingContact->firstName;
  }
  
  if(isset($billingContact->phone) ){
    $billingContact->phone = is_null($billingContact->phone) ? $shippingContact->phone : $billingContact->phone;
  }else{
    $billingContact->phone = $shippingContact->phone;
  }
  
  if(isset($billingContact->email) ){
    $billingContact->email = is_null($billingContact->email) ? $shippingContact->email : $billingContact->email;
  }else{
    $billingContact->email = $shippingContact->email;
  }

  $drupal_uid = createNewUser($billingContact->email);

  // Работа с заказом
  $new_order = false;
  $external_id = explode('_', $token->cart->externalId);

  if(count($external_id) == 3){
    if(is_numeric($external_id[0])){
      $new_order = commerce_order_load($external_id[0]);
      $new_order->status = 'pending';
    }
  }

  if(!is_object($new_order)){ // Заказ из карточки товара

    $new_order = commerce_order_new($drupal_uid, 'pending');
    commerce_order_save($new_order);

    // Создаём line item
    foreach($items as $item){
      $product = commerce_product_load($item->productId);
      
      if($product == false){
        $return = [
          'error' => [
          'reasonCode' => 'ORDER_DETAILS_MISMATCH',
            'reason' => t('Product not found when creating order'),
          ],
        ];

        $output = $YandexPayAPI->yandexOrderCreate($return);

        drupal_json_output($output);
        break;
      }

      $line_item = commerce_product_line_item_new($product, $item->quantity->count, $new_order->order_id);
      commerce_line_item_save($line_item);
      
      // Прикрепляем line item к заказу
      $new_order->commerce_line_items['und'][] = (array)$line_item;
    }    
  }
  
  $new_order->uid = $drupal_uid;

  // Прикрепляем профили клиента
  if($token->metadata != 'nativ_payment'){
    $billing_profile  = null;
    $shipping_profile = null;
    $profiles = commerce_customer_profile_load_multiple([], ['uid' => $drupal_uid]);

    // Если профили найдены, пытаемся получить платежный и профиль доставки
    if(count($profiles) > 0){
      foreach($profiles as $profile){
        if($profile->type == 'billing'){
          $billing_profile = $profile;
        }
        
        if($profile->type == 'shipping'){
          $shipping_profile = $profile;
        }
      }
    }

    if(is_null($billing_profile)){ // Профиль оплаты
      $billing_profile = commerce_customer_profile_new('billing', $drupal_uid);

      $field = field_info_field('commerce_customer_address');
      $instance = field_info_instance('commerce_customer_profile', 'commerce_customer_address', 'billing');
      
      $values = [
        'name_line' => $billingContact->lastName.' '.$billingContact->firstName,
        'postal_code' => $shipping->zip,
        'locality' => $shipping->locality,
        'thoroughfare' => $thoroughfare,
        'first_name' => $billingContact->firstName,
        'last_name' => $billingContact->lastName,
      ];

      $values = array_merge(addressfield_default_values($field, $instance), $values);
      $values['data'] = serialize($values['data']);
      
      $billing_profile->commerce_customer_address[LANGUAGE_NONE][] = $values;
      commerce_customer_profile_save($billing_profile); 
    }
    
    if(is_null($shipping_profile)){ // Профиль доставки
      $shipping_profile = commerce_customer_profile_new('shipping', $drupal_uid);
      $field = field_info_field('commerce_customer_address');
      $instance = field_info_instance('commerce_customer_profile', 'commerce_customer_address', 'shipping');
      
      $values = [
        'name_line' => $shippingContact->lastName.' '.$shippingContact->firstName,
        'postal_code' => $shipping->zip,
        'locality' => $shipping->locality,
        'thoroughfare' => $thoroughfare,
        'first_name' => $shippingContact->firstName,
        'last_name' => $shippingContact->lastName,
      ];

      $values = array_merge(addressfield_default_values($field, $instance), $values);
      $values['data'] = serialize($values['data']);
      
      $shipping_profile->commerce_customer_address[LANGUAGE_NONE][] = $values;
      commerce_customer_profile_save($shipping_profile); 
    }

    $new_order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id'] = $billing_profile->profile_id;
    $new_order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id'] = $shipping_profile->profile_id;
  }

  // Комментарий к заказу
  if(!is_null($shipping->comment)){
    $comment_field = YP_SETTINGS_RAW['comment_field'];
    $new_order->{$comment_field}[LANGUAGE_NONE][0]['value'] = $shipping->comment;
  }

  // Указываем стоимость доставки
  $shipping_amount = $token->shippingMethod->courierOption->amount;

  if(is_null($shipping_amount) || !is_numeric($shipping_amount)){
    $shipping_amount = 0;
  }
  
  $unit_price = [
    'amount' => commerce_currency_decimal_to_amount($shipping_amount, $token->currencyCode),
    'currency_code' => $token->currencyCode,
    'data' => [],
  ];

  $line_item = commerce_shipping_service_rate_calculate('yandex_shipping_service', $unit_price, $new_order->order_id);
  commerce_shipping_add_shipping_line_item($line_item, $new_order, true);

  $order_wrapper = commerce_cart_order_refresh($new_order);

  if($token->metadata != 'nativ_payment'){
    commerce_checkout_complete($new_order);
  }

  $order_ids = variable_get('commerce_yandexpay_order_ids', []);  
  $order_ids[$order_wrapper->order_id->value()] = $order_wrapper->order_id->value();
  variable_set('commerce_yandexpay_order_ids', $order_ids);

  $return = [
    'order_id' => $order_wrapper->order_id->value(),
    'receipt' => null, 
  ];
    
  saveYPOrderId($return['order_id']);

  $output = $YandexPayAPI->yandexOrderCreate($return);

  drupal_json_output($output);
}

/*
 *  Изменении статуса заказа
 *  /v1/webhook
 *
*/
function wrapperYandexWebhook(){
  global $yp_commerce_currency;

  $content = trim(file_get_contents('php://input'));
  $YandexPayAPI = new YandexPayAPI(YP_SETTINGS_RAW);
  $token = $YandexPayAPI->JWTValidation($content);

  if(isset($token->api_message)){ // Валидация токена не успешна
    drupal_add_http_header('Status', $token->api_message->reason, TRUE);
    drupal_json_output($token->api_message);   
  }

  $order = false;
  $order = commerce_order_load($token->order->orderId);

  $data = [
    'error' => '',
    'token' => $token,
  ];
  
  if($order == false){
    $data['error'] = 'ORDER_NOT_FOUND';
    $output = $YandexPayAPI->yandexOrderWebhook($data);
    drupal_json_output($output);
  }

  // Изменение статуса платежа или доставки
  if($token->event == 'ORDER_STATUS_UPDATED'){
    $transaction = commerce_payment_transaction_new('commerce_yandexpay', $token->order->orderId);
    $transaction->amount = $order->commerce_order_total[LANGUAGE_NONE][0]['amount']; // Сумма заказа в копейках
    $transaction->currency_code = $order->commerce_order_total[LANGUAGE_NONE][0]['currency_code']; 
    $transaction->status = 'success'; // success/failure
    $transaction->message = t('@description <br>merchantId: @merchantId <br>eventTime: @eventTime');
    $transaction->message_variables = [
      '@description' => '',
      '@merchantId' => $token->merchantId,
      '@eventTime' => $token->eventTime,
    ];

    switch ($token->order->paymentStatus) {
      case 'PENDING':
        $transaction->amount = 0;
        $transaction->remote_status = $token->order->paymentStatus;
        $transaction->message_variables['@description'] = t('Payment is expected');
        commerce_payment_transaction_save($transaction);
        break;
      case 'AUTHORIZED':
        $transaction->amount = 0;
        $transaction->remote_status = $token->order->paymentStatus;
        $transaction->message_variables['@description'] = t('The payment for the order is authorized. Funds are blocked on the payers account');
        commerce_payment_transaction_save($transaction);
        wrapperYandexDeliveryordersCapture($token->order->orderId);
        break;
      case 'CAPTURED':        
        $transaction->remote_status = $token->order->paymentStatus;
        $transaction->message_variables['@description'] = t('The order is successfully paid. Funds are written off from the payers account');
        commerce_payment_transaction_save($transaction);
        break;
      case 'VOIDED':
        $transaction->amount = 0;
        $transaction->remote_status = $token->order->paymentStatus;
        $transaction->message_variables['@description'] = t('Payment is canceled. No write -off of funds was made');
        commerce_payment_transaction_save($transaction);
        break;
      case 'REFUNDED':
        $transaction->amount = 0;
        $transaction->remote_status = $token->order->paymentStatus;
        $transaction->message_variables['@description'] = t('Return of funds for order');
        commerce_payment_transaction_save($transaction);
        break;
      case 'PARTIALLY_REFUNDED':
        $transaction->amount = 0;
        $transaction->remote_status = $token->order->paymentStatus;
        $transaction->message_variables['@description'] = t('Partial return of funds for order');
        commerce_payment_transaction_save($transaction);
        break;
      case 'FAILED':
        $transaction->amount = 0;
        $transaction->remote_status = $token->order->paymentStatus;
        $transaction->message_variables['@description'] = t('The order was not successfully paid');
        commerce_payment_transaction_save($transaction);
        break;       
    } 
  }

  $output = $YandexPayAPI->yandexOrderWebhook($data);

  drupal_json_output($output);
}

/*
 *  Получение точек самовывоза
 *  /v1/pickup-options
 *
 */
function wrapperYandexPickupOptions(){
  $content = trim(file_get_contents('php://input'));
  $YandexPayAPI = new YandexPayAPI(YP_SETTINGS_RAW);
  $token = $YandexPayAPI->JWTValidation($content);

  if(isset($token->api_message)){ // Валидация токена не успешна
    drupal_add_http_header('Status', $token->api_message->reason, TRUE);
    drupal_json_output($token->api_message);   
  }
  
  $data = [
    'error' => 'OTHER'
  ];  

  $output = $YandexPayAPI->yandexPickupOptions($data);
  drupal_json_output($output);
}

/*
 *  Получение точек самовывоза
 *  /v1/pickup-option-details
 *
 */
function wrapperYandexPickupOptionsDetails(){
  $content = trim(file_get_contents('php://input'));
  $YandexPayAPI = new YandexPayAPI(YP_SETTINGS_RAW);
  $token = $YandexPayAPI->JWTValidation($content);

  if(isset($token->api_message)){ // Валидация токена не успешна
    drupal_add_http_header('Status', $token->api_message->reason, TRUE);
    drupal_json_output($token->api_message);   
  }
  
  $data = [
    'error' => 'OTHER'
  ];  

  $output = $YandexPayAPI->yandexPickupOptionsDetails($data);
  drupal_json_output($output);
}

/*
 *  Коррекция опций Яндекс Доставки
 *  /v1/yandex-delivery/adjust-options
 *
 */
function wrapperYandexDeliveryAdjustOptions(){
  $content = trim(file_get_contents('php://input'));
  $YandexPayAPI = new YandexPayAPI(YP_SETTINGS_RAW);
  $token = $YandexPayAPI->JWTValidation($content);

  if(isset($token->api_message)){ // Валидация токена не успешна
    drupal_add_http_header('Status', $token->api_message->reason, TRUE);
    drupal_json_output($token->api_message);   
  }
  
  $data = [
    'error' => 'OTHER'
  ];  

  $output = $YandexPayAPI->yandexDeliveryAdjustOptions($data);
  drupal_json_output($output);
}

/*
 *  Подтверждение платежа
 *  /api/merchant/v1/orders/{orderId}/capture
 *
 */
function wrapperYandexDeliveryordersCapture($order_id){
  $attempt = 0;
  $YandexPayAPI = new YandexPayAPI(YP_SETTINGS_RAW);
  $order = commerce_order_load($order_id);
  
  if($order == false){
    watchdog('Yandex Pay', t('Order No. @order_id was not found. Payment has not passed'), [
      '@order_id' => $order_id
    ], WATCHDOG_INFO);
    drupal_set_message(t('Order was not found. Payment has not passed. Details in the System Journal'), 'error');
    
    return false;
  }

  while ($attempt <= 5) {   
    $attempt++;
    $result = $YandexPayAPI->yandexDeliveryordersCapture($order_id, $attempt);

    if($result->code == 200){
      break;
    }
    
    if($result->code == 400){
      $data = json_decode($result->data);
      watchdog('Yandex Pay', t('@status<br>@reasonCode<br>@reason'), [
        '@status' => $data->status,
        '@reasonCode' => $data->reasonCode,
        '@reason' => $data->reason,
      ], WATCHDOG_INFO);
      drupal_set_message(t('Payment error. Payment has not passed. Details in the System Journal'), 'error');
      
      return false;
    }
    
    if($result->code == 404){    
      watchdog('Yandex Pay', t('Order No. @order_id was not found. Payment has not passed'), [
        '@order_id' => $order_id
      ], WATCHDOG_INFO);
      drupal_set_message(t('Order was not found. Payment has not passed. Details in the System Journal'), 'error');
      
      return false;
    }  
 
  }
  
  return true;
}


