<?php

function createTempOrderID($id) {
  $length = 10;
  $chars='qazxswedcvfrtgbnhyujmkiolp1234567890'; 
  $size = StrLen($chars)-1; 
  $output = ''; 

  while($length--){
    $output .= $chars[rand(0,$size)]; 
  }

  return $id.'_'.time().'_'.$output;
}

function preparingSettings($settings) {
  $allowed = [    
    'shop_name', 'domains', 'callback_url', 'merchant_id',
    'btn_view_product_card', 'btn_view_cart', 'btn_view_checkout',
    'btn_color', 'btn_width', 'btn_class', 'btn_margin_top', 'btn_margin_right', 'btn_margin_bottom', 'btn_margin_left',
    'delivery_name', 'delivery_amount', 'delivery_date', 'key_api', 'working_mode'
  ];
  
  foreach($settings as $key => $name){
    if(!in_array($key, $allowed)){
      unset($settings[$key]);
    }
  }

  return $settings;
}

function createNewUser($mail){
  
  $mail = filter_xss($mail);

  // Убеждаемся, что пользователя нет среди зарегистрированных
  $query = db_select('users', 'u')
    ->fields('u', ['uid'])
    ->condition('u.mail', $mail)
    ->execute();
  $result = $query->fetch();

  if(isset($result->uid)) { 
    return $result->uid;
  }
  
  $pass = user_password();

  $account = new stdClass();
  $account->name = $mail;
  $account->mail = $mail;
  $account->init = $mail;
  $account->pass = $pass;
  $account->status = 1;
  
  $account = user_save($account);
  _user_mail_notify('register_admin_created', $account);
  
  if ($account->uid) {
    watchdog('Yandex Pay', t('New user created: email - @mail, password - @password'), [
      '@mail' => $mail,
      '@password' => $pass
    ], WATCHDOG_INFO);
    
    return $account->uid;
  } 
}

function getOrderFieldList(){
  $output = ['empty'=>t('Не выбрано')];
  $fields = field_info_instances('commerce_order', 'commerce_order');
  
  if(is_array($fields)){
    foreach($fields as $key=>$value){
      $output[$key] = $value['label'];
    }
  }
  
  return $output;
}

function yandexpayAjaxOrderSuccess() {
  $output = ['error' => ''];
  
  if(isset($_POST['orderId'])){
    $order_id = filter_xss($_POST['orderId']);    
    $order = commerce_order_load($order_id);
    
    if($order != false){      
      $output['url'] = commerce_checkout_order_uri($order);
    } 
  }  

  drupal_json_output($output);
}

function removeAllOrders(){
  $orders = commerce_order_load_multiple([], ['type' => 'commerce_order']);

  if(count($orders) > 0){
    foreach($orders as $order_id=>$order){
      commerce_order_delete($order_id);
    }
  } 
}

function removeAllProfiles(){
  $profiles = commerce_customer_profile_load_multiple([], ['status' => 1]);
 
  if(count($profiles) > 0){
    foreach($profiles as $profile_id=>$profile){
      commerce_customer_profile_delete($profile_id);
    }
  } 
}

function getOrderListForRefund(){
	$q = db_select('commerce_order', 'co');
	$q->fields('co', ['yp_order_id']);
	$res = $q->execute();		
	
	$output =[];
  
	while($rec = $res->fetchAssoc()){
    if($rec['yp_order_id'] != ''){
      $output[] = $rec['yp_order_id'];
    }
	}	

  return $output;
}

function saveYPOrderId($yp_order_id){
	$up = db_update('commerce_order');
	$up->fields(array(
		'yp_order_id'		=> $yp_order_id,			
	));
	$up->condition('order_id', $yp_order_id, '=');
	$up->execute();
}  