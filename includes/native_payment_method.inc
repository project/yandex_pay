<?php

function commerce_yandexpay_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_yandexpay'] = array(
    'title' => t('Yandex Pay'),
    'active' => TRUE,
  );

  return $payment_methods;
}

function commerce_yandexpay_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  return true;
}

function commerce_yandexpay_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  return $form['yandex-pay-selected'] = ['#type' => 'hidden', '#name' => 'yandex-pay-selected','#value' => 1];
}

function commerce_yandexpay_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  commerce_yandexpay_transaction($payment_method, $order, $charge);
}

function commerce_yandexpay_transaction($payment_method, $order, $charge) {
  return null;
}