<?php

function commerce_yandexpay_field_info() {
  return array(
    'yandex_button' => array(
      'label' => t('Payment button Yandex'),
      'description' => t('Yandex Pay or Yandex Pay Checkout buttons'),
      'settings' => [],
      'instance_settings' => [],
      'default_widget' => 'yandex_button_widget',
      'default_formatter' => 'yandex_pay',
    ),
  );
}

function commerce_yandexpay_field_is_empty($item, $field) {
  return FALSE;
}

function commerce_yandexpay_field_widget_info_alter(&$info) {
  $widgets = [
    'yandex_button_widget' => ['yandex_button'],
  ];
  
  foreach ($widgets as $widget => $field_types) {
    $info[$widget]['field types'] = array_merge($info[$widget]['field types'], $field_types);
  }
}

function commerce_yandexpay_field_widget_info() {
  return array(
    'yandex_button_widget' =>[
      'label' => t('Payment button Yandex'),
      'field types' => [
        'yandex_button',
      ],
      'settings' => [],
    ],
  );
}

function commerce_yandexpay_field_formatter_info() {
  return [
    'yandex_pay' => [
      'label' => t('Yandex Pay'),
      'field types' => ['yandex_button'],
    ],
  ];
}

function commerce_yandexpay_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, &$items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'yandex_pay':  
      $items[0] = ['value' => 'element'];
      $element[0] = ['#theme' => 'yandex_pay_field_formatter'];
      break;
  }
  return $element;
}


