<?php

function commerce_yandexpay_commerce_shipping_method_info() {
  $shipping_methods = array();

  $shipping_methods['yandex_shipping_method'] = array(
    'title' => t('Yandex shipping method'),
    'description' => t('Do not use this method on the order form. The purpose of this method is to accept delivery details in the form of Yandex Pay'),
  );

  return $shipping_methods;
}

function commerce_yandexpay_commerce_shipping_service_info() {
  $shipping_services = array();

  $shipping_services['yandex_shipping_service'] = array(
    'title' => t('Yandex shipping method'),
    'description' => t('Do not use this method on the order form. The purpose of this method is to accept delivery details in the form of Yandex Pay'),
    'display_title' => t('Yandex shipping'),
    'shipping_method' => 'yandex_shipping_method',
    'price_component' => 'shipping',
    'rules_component' => false,
    'callbacks' => array(
      'rate' => 'commerce_yandexpay_service_rate',      
    ),
  );

  return $shipping_services;
}

function commerce_yandexpay_service_rate($shipping_service, $order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  return array(
    'amount' => 2000,
    'currency_code' => $order_wrapper->commerce_order_total->currency_code->value(),
    'data' => array(),
  );
}

function commerce_yandexpay_commerce_price_component_type_info() {
  return array(
    'yandex_shipping_method_express' => array(
      'title' => t('Yandex delivery'),
      'weight' => 20,
    ),
  );
}
