<?php

use Firebase\JWT\JWT;
use Firebase\JWT\JWK;
use Firebase\JWT\Key;

class YandexPayAPI{  

  private const PUBLIC_KEY_URL = '/api/jwks';
  private const REQUEST_TIMEOUT = 15000;

  public $yandex_settings;
  public $receipt = null;
  public $shipping_receipt = null;  

  /*
   * $yandex_settings - array
   * 
   * Обязательные элементы массива
   *  $yandex_settings['merchant_id ']
  */
  public function __construct($yandex_settings){
    
    $yandex_settings['merchant_id'] = isset($yandex_settings['merchant_id']) ? $yandex_settings['merchant_id'] : null;
    
    $this->yandex_settings = $yandex_settings;
    
    $this->yandex_settings['delivery_name'] = isset($this->yandex_settings['delivery_name']) ? $this->yandex_settings['delivery_name'] : t('Customer Delivery Service');
    $this->yandex_settings['delivery_amount'] = isset($this->yandex_settings['delivery_amount']) ? $this->yandex_settings['delivery_amount'] : 0;
    $this->yandex_settings['delivery_date'] = isset($this->yandex_settings['delivery_date']) ? $this->yandex_settings['delivery_date'] : 0; 
  
    $this->yandex_settings['delivery_amount'] = number_format($this->yandex_settings['delivery_amount'], 2, '.', '');

    $date = strtotime('+'.$this->yandex_settings['delivery_date'].' day', time());
    $this->yandex_settings['delivery_date'] = date('Y-m-d', $date); 
    
    if($this->yandex_settings['fns_active'] == 'yes'){
      $this->receipt = $this->createReceipt();
      $this->shipping_receipt = $this->createReceipt('shipping');
    }
  }

  /*
   * Валидация JWT токена присланного от Яндекса
   *
  */
  public function JWTValidation($jwt = ''){
    $timestamp = \time();
    
    $token = explode('.', $jwt);
    
    // Проверяем, что токен состоит из 3 частей
    if(count($token) !== 3){
      return $this->errorMessages(403, 'Token not received or received with an error');
    }
    
    // Получаем публичные JWK-ключи и выбираем нужный на основании
    //   alg - алгоритм подписи
    //   kid - ID ключа 
    $public_key_data = [];
    $public_keys = json_decode(file_get_contents($this->yandex_settings['api_url'] . self::PUBLIC_KEY_URL));
    $token_header = json_decode(base64_decode($token[0]));

    foreach($public_keys->keys as $key){
      if($key->alg == $token_header->alg && $key->kid == $token_header->kid){
        
        foreach($key as $k=>$v){
          $public_key_data[$k] = $v;
        }
        
        break;
      }
    }  
    
    if(count($public_key_data) == 0){
      return $this->errorMessages(403, 'Invalid public key');
    }

    // Проверка JWK ключа на его параметры
    try {
      $keyObj = JWK::parseKey($public_key_data);
    }
    catch (InvalidArgumentException $e) {
      return $this->errorMessages(403, $e->getMessage());
    }
    catch (UnexpectedValueException $e) {
      return $this->errorMessages(403, $e->getMessage());
    }
    catch (DomainException $e) {
      return $this->errorMessages(403, $e->getMessage());
    }

    // Валидация JWT токена
    try {
      $decoded = JWT::decode($jwt, $keyObj);
    }
    catch (InvalidArgumentException $e) {
      return $this->errorMessages(403, $e->getMessage());
    }
    catch (UnexpectedValueException $e) {
      return $this->errorMessages(403, $e->getMessage());
    }
    catch (SignatureInvalidException $e) {
      return $this->errorMessages(403, $e->getMessage());
    }
    catch (BeforeValidException $e) {
      return $this->errorMessages(403, $e->getMessage());
    }
    catch (ExpiredException $e) {
      return $this->errorMessages(403, $e->getMessage());
    }
    catch (DomainException $e) {
      return $this->errorMessages(403, $e->getMessage());
    }
    
    /*
     * Библиотека "firebase/php-jwt" проверяет iat и exp в payload токена
     * у нас же эти параметры могут быть в заголовке.
     * Проверяем их в заголовке.
    */
    if (isset($token_header->iat) && $token_header->iat > $timestamp) {
      return $this->errorMessages(403, 'Cannot handle token prior to ' . \date(DateTime::ISO8601, $token_header->iat));
    }

    if (isset($token_header->exp) && $timestamp >= $token_header->exp) {
      return $this->errorMessages(403, 'Expired token');
    }

    // Проверка мерчанта
    if (!isset($decoded->merchantId)) {
      return $this->errorMessages(403, 'merchantId empty');
    }
    
    if (is_null($this->yandex_settings['merchant_id']) || 
        $this->yandex_settings['merchant_id'] != $decoded->merchantId) {
      return $this->errorMessages(403, 'merchantId does not match');
    }    

    return $decoded;
  }
 
  public function yandexOrderRender($store_settings, $cart, $shipping){

    $items = [];
 
    foreach($cart['items'] as $item){

      $items[] = (Object)[
        'productId' =>  $item['productId'],
        'unitPrice' =>  $item['unitPrice'],                     # Полная цена за единицу товара
        'discountedUnitPrice' =>  $item['discountedUnitPrice'], # Цена за единицу товара с учетом скидок
        'subtotal' =>  $item['subtotal'],                       # Суммарная цена за позицию без учета скидок
        'total' =>  $item['total'],                             # Суммарная цена за позицию со всеми скидками
        'title' =>  $item['title'],                             # Название товара
        'quantity' =>  (Object)[
          'count' =>  $item['qty_count'],                       # Количество товара в заказе
        ],
        'receipt' => $this->receipt                             // Опционально. Данные для отправки чека в ФНС
      ];
    }
    
    // Доставка по умолчанию
    $availableCourierOptions = [];
    
    $availableCourierOptions[] = (Object)[
      'courierOptionId' => 'custom',
      'provider' => 'COURIER', 
      'category' => 'STANDARD',
      'title' => is_null($shipping) ? $this->yandex_settings['delivery_name'] : $shipping['title'],
      'amount' => is_null($shipping) ? $this->yandex_settings['delivery_amount'] : $shipping['amount'],
      'fromDate' => $this->yandex_settings['delivery_date'],
      'receipt' => $this->shipping_receipt                       // Опционально. Данные для отправки чека в ФНС
    ];    

    $output = (Object)[
      'status' => 'success',
      'data' => (Object)[
        'currencyCode' => $store_settings['currencyCode'],
        'availablePaymentMethods' => ['CARD', 'SPLIT', 'CASH_ON_DELIVERY', 'CARD_ON_DELIVERY'],
        'enableCoupons' =>  false, 
        'enableCommentField' =>  true, 

        'requiredFields' =>  (Object)[            # Поля, запрашиваемые у пользователя на форме оплаты
          'billingContact' =>  (Object)[
            'name' =>  true,
            'email' =>  true
          ],
          'shippingContact' =>  (Object)[
            'name' =>  true,
            'email' =>  true,
            'phone' =>  true
          ],
        ],
        
        'cart' =>  (Object)[
          'externalId' =>  $cart['externalId'],
          'items' =>  $items,
          'total' =>  (Object)[                          # Суммарная стоимость корзины (без учёта доставки)
            'amount' =>  $cart['cart_total'],
            'label' =>  null
          ],
        ],
        
        'shipping' => (Object)[
          'availableMethods' => ['COURIER'],
          'availableCourierOptions' =>  $availableCourierOptions
        ] 
      ]
    ]; 

    return $output;  
  }
  
  public function yandexOrderCreate($order){
   
    if(!isset($order['error'])){
      $output = (Object)[
        'status' => 'success',
        'data' => (Object)[
          'orderId' => strval($order['order_id'])  // Созданный ID заказа на стороне продавца.
        ]
      ];
    }else{
      $output = (Object)[
        'status' => 'fail',
        'reasonCode' => $order['error']['reasonCode'], // код ошибки
        'reason' => $order['error']['reason'],         // текстовое описание ошибки
      ];
    }

    return $output;
  }
  
  public function yandexOrderWebhook($data){
   
    if($data['error'] == 'ORDER_NOT_FOUND'){
      $output = (Object)[
        'status' => 'fail',
        'reasonCode' => 'ORDER_NOT_FOUND',
        'reason' => t('The order is not found'),
      ];
      
      return $output;
    }   
   
    $output = (Object)[
      'status' => 'success'
    ];

    return $output;
  }

  public function yandexPickupOptions($data){
   
    if($data['error'] == 'OTHER'){
      $output = (Object)[
        'status' => 'fail',
        'reasonCode' => $data['error'],
        'reason' => t('The module does not support this method'),
      ];
      
      return $output;
    }  
  }

  public function yandexPickupOptionsDetails($data){
   
    if($data['error'] == 'OTHER'){
      $output = (Object)[
        'status' => 'fail',
        'reasonCode' => $data['error'],
        'reason' => t('The module does not support this method'),
      ];
      
      return $output;
    }  
  }

  public function yandexDeliveryAdjustOptions($data){
   
    if($data['error'] == 'OTHER'){
      $output = (Object)[
        'status' => 'fail',
        'reasonCode' => $data['error'],
        'reason' => t('The module does not support this method'),
      ];
      
      return $output;
    }  
  }

 public function createReceipt($type = 'product'){

    $output = (Object)[];
    
    if($this->yandex_settings['fns_title'] != ''){
      $output->{'title'} = $this->yandex_settings['fns_title'];
    }
    
    if($this->yandex_settings['fns_tax'] == 'no' || 
      $this->yandex_settings['fns_measure'] == 'no'){
      return null;
    }
    $output->{'tax'} = intval($this->yandex_settings['fns_tax']);
    $output->{'measure'} = intval($this->yandex_settings['fns_measure']);
    
    if($this->yandex_settings['fns_paymentMethodType'] != 'no'){
      $output->{'paymentMethodType'} = intval($this->yandex_settings['fns_paymentMethodType']);
    }
    
    if($this->yandex_settings['fns_paymentSubjectType'] != 'no'){
      $output->{'paymentSubjectType'} = intval($this->yandex_settings['fns_paymentSubjectType']);
    }
    
    if($this->yandex_settings['fns_excise'] != ''){
      $output->{'excise'} = number_format($this->yandex_settings['fns_excise'], 2, '.', ''); 
    }    
    
    if($this->yandex_settings['fns_productCode'] != ''){
      $code = mb_substr(base64_encode($this->yandex_settings['fns_productCode']), 0, 31);
      $output->{'productCode'} = $code;
    } 
    
    if($this->yandex_settings['fns_markQuantity_1'] != '' && 
      $this->yandex_settings['fns_markQuantity_2'] != ''){
      $output->{'markQuantity'} = (Object)[
        'numerator' => intval($this->yandex_settings['fns_markQuantity_1']),
        'denominator' => intval($this->yandex_settings['fns_markQuantity_2']),
      ];
    }
    
    if($this->yandex_settings['fns_supplier_name'] != ''){
      $output->{'supplier'} = (Object)[
        'name' => $this->yandex_settings['fns_supplier_name'],
        'inn' => $this->yandex_settings['fns_supplier_inn'],
        'phones' => explode(',', $this->yandex_settings['fns_supplier_phones']),
      ];      
    }
    
    if($this->yandex_settings['fns_agent_type'] != 'no'){
      $output->{'agent'} = (Object)[
        'agentType' => intval($this->yandex_settings['fns_agent_type']),
        'operation' => $this->yandex_settings['fns_agent_operation'],
        'phones' => explode(',', $this->yandex_settings['fns_agent_phones']),
        'transferOperator' => (Object)[
          'name' => $this->yandex_settings['fns_agent_transferOperator_name'],
          'address' => $this->yandex_settings['fns_agent_transferOperator_address'],
          'inn' => $this->yandex_settings['fns_agent_transferOperator_inn'],
          'phones' => explode(',', $this->yandex_settings['fns_agent_transferOperator_phones']),
        ],
      ];      
    }
    
    if($this->yandex_settings['fns_agent_paymentsOperator_phones'] != ''){
      $output->{'paymentsOperator'} = (Object)[
        'phones' => explode(',', $this->yandex_settings['fns_agent_paymentsOperator_phones']),
      ];      
    }    
    
    if($type == 'shipping'){
      $output->measure = 0;
      $output->{'paymentSubjectType'} = 4;    
    }

    return $output;  
  } 
  
  /*
   * Запросы из модуля в Яндекс
   *
  */  
  public function yandexDeliveryordersCapture($order_id = 0, $attempt = 1){

    $url = $this->yandex_settings['api_url']."/api/merchant/v1/orders/$order_id/capture";
    $data = [];

    if ($curl = curl_init()) {
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_TIMEOUT, 15);
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Authorization:'.'Api-Key '.$this->yandex_settings['key_api'],
          'X-Request-Id:'.$order_id,
          'X-Request-Timeout:'.self::REQUEST_TIMEOUT,
          'X-Request-Attempt:'.$attempt,
      ));

      $out = curl_exec($curl);
      curl_close($curl);

      return json_decode($out);
    }else{
      throw new Exception('cURL - не установлен');
    }   
  }

  public function yandexGetOrderInfo($order_id){
    $url = $this->yandex_settings['api_url']."/api/merchant/v1/orders/$order_id";
    $data = [];

    if ($curl = curl_init()) {
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Authorization:'.'Api-Key '.$this->yandex_settings['key_api'],
          'X-Request-Id:'.$order_id,
          'X-Request-Timeout:'.self::REQUEST_TIMEOUT,
          'X-Request-Attempt: 1',
      ));

      $out = curl_exec($curl);
      curl_close($curl);

      return json_decode($out);
    }else{
      throw new Exception('cURL - не установлен');
    } 
  }

  public function yandexRefund($oldOrder, $newOrder){    
    $cart = $oldOrder->cart;
    $shipping = $oldOrder->shippingMethod;
    $new_order_amount = 0;
    $old_order_amount = $oldOrder->orderAmount;
    $cart->total->amount = 0;
    $order_id = $newOrder->orderId;   
    
    $url = $this->yandex_settings['api_url']."/api/merchant/v1/orders/$order_id/refund";

    $outputCart = (object)[
      'refundAmount' => 0,
      'orderAmount' => 0,
      'cart' => null,
      'shipping' => (object)[
        'methodType' => $shipping->methodType,
        'amount' => $shipping->courierOption->amount,
        'receipt' => isset($shipping->courierOption->receipt) ? $shipping->courierOption->receipt : null
      ]
    ];

    foreach($newOrder->items as $pos => $item){
      $newItem = $newOrder->items[$pos];

      if($newItem->type == 'product'){       
        if($cart->items[$pos]->unitPrice > $newItem->unitPrice){
          $cart->items[$pos]->unitPrice = $newItem->unitPrice;
        }
        
        if($cart->items[$pos]->discountedUnitPrice > $newItem->discountedUnitPrice){
          $cart->items[$pos]->discountedUnitPrice = $newItem->discountedUnitPrice;
        }
        
        if($cart->items[$pos]->subtotal > $newItem->subtotal){
          $cart->items[$pos]->subtotal = $newItem->subtotal;
        }
        
        if($cart->items[$pos]->total > $newItem->total){
          $cart->items[$pos]->total = $newItem->total;
        }
        
        if($cart->items[$pos]->quantity->count > $newItem->qty){
          $cart->items[$pos]->quantity->count = $newItem->qty;
        }         
        
        $cart->total->amount += $cart->items[$pos]->total;
        $new_order_amount += $cart->items[$pos]->total;
      }

      if($newItem->type == 'shipping'){
        if($shipping->courierOption->amount > $newItem->shipping){
          $outputCart->shipping->amount = $newItem->shipping;          
        }
        
        $new_order_amount += $outputCart->shipping->amount;
      }      
    }
    
    if(isset($cart->discounts[0])){
      //$new_order_amount -= $cart->discounts[0]->amount;
    }
    
    $outputCart->refundAmount = number_format($old_order_amount - $new_order_amount, 2, '.', '');
    $outputCart->orderAmount = number_format($new_order_amount, 2, '.', '');    
    $outputCart->cart = $cart;

    if ($curl = curl_init()) {
      $outputCart = json_encode($outputCart);
    
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $outputCart);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Authorization:'.'Api-Key '.$this->yandex_settings['key_api'],
          'X-Request-Id:'.$order_id,
          'X-Request-Timeout:'.self::REQUEST_TIMEOUT,
          'X-Request-Attempt: 1',
      ));    

      $out = curl_exec($curl);
      curl_close($curl);
      
      $result = json_decode($out);

      if($result->status == 'success'){
        return ['reasonCode' => false];
      }else{
        return [
          'reasonCode' => $result->reasonCode.' - '.$result->reason,
        ];
      }

    }else{
      throw new Exception('cURL - не установлен');
    }  
  } 

  public function errorMessages($code, $message){
    $output = (Object)[
      'api_message' => true,
      'message' => []
    ];
    
    switch ($code) {
      case 403:
        $output->message = (Object)[
          'code' => 403,
          'reasonCode' => 'FORBIDDEN',
          'reason' => $code.' '.$message
        ];
        break;
      case 1:
        //
        break;
    }

    return $output;
  }
  
}