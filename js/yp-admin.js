(function($) { 

  $(document).ready(function(){
  
    $('.yp-btn-refund').click(function(){
      var products = $('.items.details .item');
      var output = {
            orderId: $('#edit-order-list').val(),
            items: []
        };

      if(products.length > 0){
        products.each(function(index, element){
          var product = $(element);
          var type = 'product';
          
          if(product.hasClass('shipping')){
            type = 'shipping';
          }
          
          output.items[index] = {
            type: type,
            qty: product.find('.yp-qty input').val(),
            unitPrice: product.find('.yp-unitPrice input').val(),
            discountedUnitPrice: product.find('.yp-discountedUnitPrice input').val(),
            subtotal: product.find('.yp-subtotal input').val(),
            total: product.find('.yp-total input').val(), 
            shipping: product.find('.yp-shipping input').val()
          }
        });
      }
   
      $.ajax({
        url         : '/commerce_yandexpay__ajax__order_refund',
        type        : 'POST',
        data        : { orderRefund: JSON.stringify(output) },
        dataType    : 'json',       
        success     : function( respond, status, jqXHR ){

          if(respond.result.reasonCode == false){
            $('.yp-inf-refund').html(refundSuccessfully);
          }else{
            $('.yp-inf-refund').html(respond.result.reasonCode);
          }
          
          $('.yp-inf-refund').show();
        },
        error: function( jqXHR, status, errorThrown ){
          console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
        }
      });
    });
    
    $('#edit-order-list').change(function() {

      $.ajax({
        url         : '/commerce_yandexpay__ajax__get_order_info',
        type        : 'POST',
        data        : { orderId: $(this).val() },
        dataType    : 'json',       
        success     : function( respond, status, jqXHR ){
          var order = respond.data.order;

          if(order != null){
            var clone = undefined;
            var tmpItem = $('.item.tmp');
            var cart = order.cart;
            var shipping = order.shippingMethod.courierOption;
            var itemsWrap = $('.items.details');
            itemsWrap.html('');
            $('.yp-inf-refund').html('');
            
            // Товар
            for (key in cart.items) {
              clone = tmpItem.clone();
              clone.find('.yp-name').html(cart.items[key].title);
              clone.find('.yp-qty input').val(cart.items[key].quantity.count);
              
              clone.find('.yp-unitPrice input').val(cart.items[key].unitPrice);
              clone.find('.yp-discountedUnitPrice input').val(cart.items[key].discountedUnitPrice);
              clone.find('.yp-subtotal input').val(cart.items[key].subtotal);
              clone.find('.yp-total input').val(cart.items[key].total);
 
              clone.removeClass('tmp');
              itemsWrap.append(clone);
            };
            
            // Доставка
            clone = tmpItem.clone();
            clone.find('.yp-name').html(txtDelivery + ': ' + shipping.title);
            clone.find('.yp-shipping input').val(shipping.amount);            
            clone.removeClass('tmp').addClass('shipping');            
            itemsWrap.append(clone);    
          }
        },
        error: function( jqXHR, status, errorThrown ){
          console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
        }
      });
	  });  
  });

})(jQuery);