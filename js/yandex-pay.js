var yandexPayCheckoutData = false;

(function($) {

  var yandexPayShow = false;
  var debugStatus = false;
  var yp_settings;

  $(window).load(function() {
    yandexPayShow = true;
  });   

  $(document).ready(function(){

    var YaPay;
    var checkoutData;
    var checkoutObject;
    var buttonTypeUsed = 'Checkout'; // Checkout / Pay
    
    var btnContainer = $("#yandex_pay_button_container");
    var ypPage = 'empty';
    var orderOneProduct;    
    var ypButtonProductCard = $('.yandex-pay-button-product-card');   

    yp_settings = Drupal.settings.yp_settings;
    var yp_currency = Drupal.settings.yp_currency;
    var yp_product = Drupal.settings.yp_product;
    var yp_any_data = Drupal.settings.yp_any_data;
    var btnView = btnViewCheck();

    if( btnContainer.length == 1){
      ypPage = btnContainer.data('page');
    }     

    if(btnView == true && yandexPayShow == false){
      var timerYP = setInterval(function(){
            if(yandexPayShow == true){
              debugMessages('Yandex: тик...');
              clearInterval(timerYP);
              startYP();              
            }
          }, 500);
    }
    
    function startYP(){
      var error = false;

      if(yp_product == undefined){
        debugMessages('Yandex: отсутствуют данные о товаре');
        error = true;
      }

      if(yp_settings.merchant_id == undefined || yp_settings.merchant_id == ''){
        debugMessages('Yandex: отсутствуют данные мерчанта');
        error = true;
      }

      if(error == false && btnContainer.length == 1){
        YaPay = window.YaPay;
        btnContainer = btnContainer[0];
        onYaPayLoad();
      }  
    }

    function onYaPayLoad() {
      const YaPay = window.YaPay;

      var CurrencyCode = 'Rub';
      
      if(yp_currency != undefined && yp_currency != ''){
        yp_currency = yp_currency.toLowerCase();
        CurrencyCode = yp_currency[0].toUpperCase() + yp_currency.slice(1);
      }
      
      if(yp_any_data == undefined){
        yp_any_data = 'empty';
      }
      
      var workingMode = 'Sandbox';
      
      if(yp_settings.working_mode == 'prod'){
        workingMode = 'Production';
      }
      
      var agent = {
        name: 'Drupal 7',
        version: '7.x-1.0'
      };
      
      var source = {
        source: ypPage // product | cart | checkout
      };      

      // Сформировать данные платежа.
      const checkoutData = {
        env: YaPay.PaymentEnv[workingMode],
        version: 3,
        currencyCode: YaPay.CurrencyCode[CurrencyCode],
        merchantId: yp_settings.merchant_id,
        cart: yp_product,
        //orderId: yp_product.externalId,
        metadata: yp_any_data,
      };

      YaPay.createSession(checkoutData, {
        agent,
        source,      
        onSuccess: onCheckoutSuccess,
        onAbort: onCheckoutAbort,
        onError: onCheckoutError,
      })
        .then(function (checkout) {
          yandexPayCheckoutData = checkout;
          createPaymentButton();
        })
        .catch(function (err) {
          debugMessages(err);
        });
        
        
    }

    // Создаем кнопку для сайта
    function createPaymentButton(){
      
      if(yp_settings.btn_class != ''){
        btnContainer.classList.add(yp_settings.btn_class);
      }
      
      var top = yp_settings.btn_margin_top + 'px ';
      var right = yp_settings.btn_margin_right + 'px ';
      var bottom = yp_settings.btn_margin_bottom + 'px ';
      var left = yp_settings.btn_margin_left + 'px ';      
      
      btnContainer.setAttribute('style', 'margin:' + top + right + bottom + left);

      yandexPayCheckoutData.mountButton(btnContainer, {
        type: YaPay.ButtonType[buttonTypeUsed],
        theme: YaPay.ButtonTheme[yp_settings.btn_color],
        width: YaPay.ButtonWidth[yp_settings.btn_width],
      }); 

      var btnNativPayment = $('.yandex-pay-button-nativ-payment');
      
      if(btnNativPayment.length > 0){
        //yandexPayCheckoutData.checkout();
      } 
    }

    if(ypButtonProductCard.length > 0){
      $('#edit-quantity').keyup(function(){          
        var qty = $(this).val();
        
        if(qty != ''){
        
          yp_product.items[0].quantity.count = qty;
          yp_product.items[0].amount = yp_product.items[0].amount * qty;
        
          var cart = {
            cart: yp_product,
            metadata: yp_any_data
          }; 
          
          if(yandexPayCheckoutData != false){
            yandexPayCheckoutData.update(cart);
            debugMessages('Yandex: корзина обновлена');
          }        
        }
      });    
    }
    
    
  });    

    function btnViewCheck(){      
      var btnProductCard = $('.yandex-pay-button-product-card');
      var btnCart = $('.yandex-pay-button-cart');
      var btnCheckout = $('.yandex-pay-button-checkout');
      
      if(btnProductCard.length > 0 && yp_settings.btn_view_product_card == 0){
        btnProductCard.remove();
        return false;
      }
      
      if(btnCart.length > 0 && yp_settings.btn_view_cart == 0){
        btnCart.remove();
        return false;
      }
      
      if(btnCheckout.length > 0 && yp_settings.btn_view_checkout == 0){
        btnCheckout.remove();
        return false;
      }
      
      return true;      
    }
    
    /*
     * Обработка событий
     *
    */
    function onCheckoutSuccess(event){
      debugMessages('Yandex Pay: onCheckoutSuccess');

      // Редирект на дефолтную страницу успешного заказа      
      $.ajax({
        url         : '/commerce_yandexpay__ajax__order_success',
        type        : 'POST',
        data        : {
          orderId: event.orderId,
          },
        dataType    : 'json',       
        success     : function( respond, status, jqXHR ){
          console.log( respond );
          
          if(respond['error'] == ''){
            if(event.metadata == "nativ_payment"){
              window.location.href = '/yandexpay-payment/order_id/' + event.orderId;
            }else{
             window.location.href = '/' + respond['url'];
            }
          }
        },
        error: function( jqXHR, status, errorThrown ){
          console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
        }
      });
    }
    
    function onCheckoutError(event){
      debugMessages('Yandex Pay: onCheckoutError');
      debugMessages(event);  
    }
    
    function onCheckoutAbort(event){
      debugMessages('Yandex Pay: onCheckoutAbort');
      debugMessages(event);
    }
    
    function onPaymentButtonClick(event){
      debugMessages('Yandex Pay: onPaymentButtonClick');
      paymentObject.checkout();
    }

    function debugMessages(text = 'debug', message = ''){
      if(debugStatus){
        console.log(text, message);
      }
    }

})(jQuery);