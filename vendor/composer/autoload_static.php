<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitec7c2e41cbbedaa987df35bd45759d68
{
    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Firebase\\JWT\\' => 13,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Firebase\\JWT\\' => 
        array (
            0 => __DIR__ . '/..' . '/firebase/php-jwt/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitec7c2e41cbbedaa987df35bd45759d68::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitec7c2e41cbbedaa987df35bd45759d68::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
