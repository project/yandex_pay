<?php
  
  
function fns_tax_list(){
  return [
    'no' => '-',
    1 => t('VAT at 20%'),
    2 => t('VAT at 10%'),
    3 => t('AT at the estimated rate of 20/120'),
    4 => t('VAT at the estimated rate of 10/110'),
    5 => t('VAT at 0%'),
    6 => t('Without VAT')
  ];  
} 

function fns_measure_list(){
  return [
    'no' => '-',
    0 => t('Pieces or Units'),
    10 => t('Gram'),
    11 => t('Kilogram'),
    12 => t('Ton'),
    20 => t('Centimeter'),
    21 => t('Decimeter'),
    22 => t('Meter'),
    30 => t('Square Centimeter'),
    31 => t('Square decimeter'),
    32 => t('Square Meter'),
    40 => t('Milliliter'),
    41 => t('Liter'),
    42 => t('Cubic Meter'),
    50 => t('Kilowatt hour'),
    51 => t('Gigacalorie'),
    70 => t('Day (day)'),
    71 => t('Hour'),
    72 => t('Minute'),
    73 => t('Second'),
    80 => t('Kilobyte'),
    81 => t('Megabyte'),
    82 => t('Gigabyte'),
    83 => t('Terabyte'),
    255 => t('Applies when using other measurement measures')
  ];  
}  

function fns_paymentMethodType_list(){
  return [
    'no' => '-',
    1 => t('Full advance payment until the transfer of the settlement item'),
    2 => t('Partial advance payment until the transfer of the settlement subject'),
    3 => t('Advance payment'),
    4 => t('Full payment at the time of transfer of the settlement subject'),
    5 => t('Partial payment for the subject of settlement at the time of its transfer, followed by payment on credit'),
    6 => t('Transfer of the subject of settlement without its payment at the time of its transfer with subsequent payment on credit'),
    7 => t('Payment for the subject of settlement after its transfer with payment on credit')
  ];  
} 

function fns_paymentSubjectType_list(){
  return [
    'no' => '-',
    1 => t('Product'),
    2 => t('Excisable goods'),
    3 => t('Work'),
    4 => t('Service'),
    5 => t('Gamble Bet'),
    6 => t('Gambling Win'),
    7 => t('Lottery ticket'),
    8 => t('Lottery Win'),
    9 => t('Provision of RIA'),
    10 => t('Payment'),
    11 => t('Agent fee'),
    12 => t('Subject Type'),
    13 => t('Subject Type 2'),
    14 => t('Property right'),
    15 => t('Non-operating income'),
    16 => t('Insurance premiums: on the amounts of expenses that reduce the amount of tax (advance payments) in accordance with paragraph 3.1 of Article 346.21 of the Tax Code of the Russian Federation'),
    17 => t('Trade Fee'),
    18 => t('Resort Fee'),
    19 => t('Deposit'),
    20 => t('Expense: about the amounts of expenses incurred in accordance with article 346.16 of the Tax Code of the Russian Federation that reduce income'),
    21 => t('Contributions for compulsory pension insurance for individual entrepreneurs'),
    22 => t('Compulsory pension insurance contributions'),
    23 => t('Compulsory medical insurance contributions for individual entrepreneurs'),
    24 => t('Compulsory health insurance premiums'),
    25 => t('Compulsory social insurance contributions'),
    26 => t('Casino Payment'),
  ];  
} 

function fns_agent_type_list(){
  return [
    'no' => '-',
    1 => t('Bank Paying Agent'),
    2 => t('Bank payment sub-agent'),
    3 => t('Paying agent'),
    4 => t('Payment sub-agent'),
    5 => t('Agent'),
    6 => t('Commission Agent'),
    7 => t('Other agent')
  ];  
}


function yandex_settings_form($form, &$form_state){

  $yandex = variable_get('yandex_settings');
// Основные настройки
  $form['main'] = [
    '#type' => 'fieldset',
    '#title' => t('Basic settings'),
    '#open' => false,
  ];
  $form['main']['shop_name'] = [
  '#type' => 'textfield',
    '#title' => t('Name of shop'),
    '#default_value' => isset($yandex['shop_name']) ? $yandex['shop_name'] : 'Example Shop',
  ];
  $form['main']['domains'] = [
    '#type' => 'textfield',
    '#title' => t('List of domains where the button will be displayed'),
    '#default_value' => isset($yandex['domains']) ? $yandex['domains'] : 'https://example.com',
  ];
  $form['main']['callback_url'] = [
    '#type' => 'textfield',
    '#title' => t('Callback URL'),
    '#default_value' => isset($yandex['callback_url']) ? $yandex['callback_url'] : 'https://example.com/example',
  ]; 
  $form['main']['merchant_id'] = [
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => isset($yandex['merchant_id']) ? $yandex['merchant_id'] : '',
  ];  
  $form['main']['key_api'] = [
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => isset($yandex['key_api']) ? $yandex['key_api'] : '',
  ];
  $form['main']['working_mode'] = [
    '#type' => 'select',
    '#title' => t('Working mode'),
    '#options' => [
      'test' => t('Test'),
      'prod' => t('Production'),
    ],
    '#default_value' => isset($yandex['working_mode']) ? $yandex['working_mode'] : 'test',
  ];     
  
// Видимость кнопки
  $form['btn_view'] = [
    '#type' => 'fieldset',
    '#title' => t('Button display'),
    '#open' => false,
  ];  
    $form['btn_view']['btn_view_product_card'] = [
      '#type' => 'checkbox',
      '#title' => t('Display in product card'),
      '#default_value' => isset($yandex['btn_view_product_card']) ? $yandex['btn_view_product_card'] : '',        
    ];
    $form['btn_view']['btn_view_cart'] = [
      '#type' => 'checkbox',
      '#title' => t('Display in cart'),
      '#default_value' => isset($yandex['btn_view_cart']) ? $yandex['btn_view_cart'] : '',        
    ];
    $form['btn_view']['btn_view_checkout'] = [
      '#type' => 'checkbox',
      '#title' => t('Display in checkout page'),
      '#default_value' => isset($yandex['btn_view_checkout']) ? $yandex['btn_view_checkout'] : '',        
    ];    
    
// Внешний вид кнопки
  $form['btn_style'] = [
    '#type' => 'fieldset',
    '#title' => t('Button style'),
    '#open' => false,
  ];
  $form['btn_style']['btn_color'] = [
    '#type' => 'select',
    '#title' => t('Button color'),
    '#options' => [
      'White' => t('White'),
      'WhiteOutlined' => t('White with outline'),
      'Black' => t('Black'),
    ],
    '#default_value' => isset($yandex['btn_color']) ? $yandex['btn_color'] : 'White',
  ];  
  $form['btn_style']['btn_width'] = [
    '#type' => 'select',
    '#title' => t('Button width'),
    '#options' => [
      'Auto' => t('Button is detected automatically'),
      'Max' => t('The button fits the full width of the container'),
    ],
    '#default_value' => isset($yandex['btn_width']) ? $yandex['btn_width'] : 'Auto',
  ]; 
  $form['btn_style']['btn_class'] = [
    '#type' => 'textfield',
    '#title' => t('CSS button container class'),
    '#default_value' => isset($yandex['btn_class']) ? $yandex['btn_class'] : '',
  ]; 
 
  $form['btn_style']['btn_style_margin'] = [
    '#type' => 'container',
  ];
  $form['btn_style']['btn_style_margin']['btn_margin_text'] = [
    '#markup' => '<b>'.t('Container padding relative to other elements').'</b>',
  ];
    $form['btn_style']['btn_style_margin']['btn_margin_top'] = [
      '#type' => 'textfield',
      '#title' => t('Top'),
      '#default_value' => isset($yandex['btn_margin_top']) ? $yandex['btn_margin_top'] : 0,
    ];
    $form['btn_style']['btn_style_margin']['btn_margin_right'] = [
      '#type' => 'textfield',
      '#title' => t('Right'),
      '#default_value' => isset($yandex['btn_margin_right']) ? $yandex['btn_margin_right'] : 0,
    ];
    $form['btn_style']['btn_style_margin']['btn_margin_bottom'] = [
      '#type' => 'textfield',
      '#title' => t('Bottom'),
      '#default_value' => isset($yandex['btn_margin_bottom']) ? $yandex['btn_margin_bottom'] : 0,
    ];
    $form['btn_style']['btn_style_margin']['btn_margin_left'] = [
      '#type' => 'textfield',
      '#title' => t('Left'),
      '#default_value' => isset($yandex['btn_margin_left']) ? $yandex['btn_margin_left'] : 0,
    ];
 
// Доставка
  $form['delivery'] = [
    '#type' => 'fieldset',
    '#title' => t('Delivery'),
    '#open' => false,
  ];
  $form['delivery']['text'] = [
    '#markup' => t('Delivery settings used in the Yandex Pay form'),
  ];
  $form['delivery']['delivery_name'] = [
    '#type' => 'textfield',
    '#title' => t('Delivery name for the customer'),
    '#default_value' => isset($yandex['delivery_name']) ? $yandex['delivery_name'] : t('Customer Delivery Service'),
  ];
  $form['delivery']['delivery_amount'] = [
    '#type' => 'textfield',
    '#title' => t('Cost of delivery'),
    '#default_value' => isset($yandex['delivery_amount']) ? $yandex['delivery_amount'] : 0,
  ];
  $form['delivery']['delivery_date'] = [
    '#type' => 'textfield',
    '#title' => t('The earliest possible delivery date'),
    '#description' => t('Enter the number of days required for delivery.<br>The client will see the date = current date + specified number of days.'),
    '#default_value' => isset($yandex['delivery_date']) ? $yandex['delivery_date'] : 0,
  ];

  $form['comment'] = [
    '#type' => 'fieldset',
    '#title' => t('Comment to the order'),
    '#open' => false,
  ];

  $comment_field = isset($yandex['comment_field']) ? $yandex['comment_field'] : '';
  $form['comment']['comment_field'] = [
    '#type' => 'select',
    '#title' => t('List of available fields in the order'),
    '#options' => getOrderFieldList(),
    '#default_value' => [$comment_field],
    '#description' => t('Select the field responsible for the comment in the order'),
  ];
  
  
// ФНС
  $form['fns'] = [
    '#type' => 'fieldset',
    '#title' => t('Sending checks to the Federal Tax Service (54-FZ)'),
    '#open' => false,
  ];  
  $form['fns']['fns_active'] = [
    '#type' => 'select',
    '#title' => t('Send a check to the FTS?'),
    '#options' => [
      'no' => t('No'),
      'yes' => t('Yes'),
    ],
    '#default_value' => isset($yandex['fns_active']) ? $yandex['fns_active'] : 'no',
  ]; 
  $form['fns']['fns_title'] = [
    '#type' => 'textfield',
    '#title' => t('Payment item name'),
    '#default_value' => isset($yandex['fns_title']) ? $yandex['fns_title'] : '',
  ];
  $form['fns']['fns_tax'] = [
    '#type' => 'select',
    '#title' => t('VAT rate'),
    '#options' => fns_tax_list(),
    '#default_value' => isset($yandex['fns_tax']) ? $yandex['fns_tax'] : 'no',
  ];
  $form['fns']['fns_measure'] = [
    '#type' => 'select',
    '#title' => t('Units code'),
    '#options' => fns_measure_list(),
    '#default_value' => isset($yandex['fns_measure']) ? $yandex['fns_measure'] : 'no',
  ];   
  $form['fns']['fns_paymentMethodType'] = [
    '#type' => 'select',
    '#title' => t('Payment method'),
    '#options' => fns_paymentMethodType_list(),
    '#default_value' => isset($yandex['fns_paymentMethodType']) ? $yandex['fns_paymentMethodType'] : 'no',
  ]; 
  $form['fns']['fns_paymentSubjectType'] = [
    '#type' => 'select',
    '#title' => t('Settlement item'),
    '#options' => fns_paymentSubjectType_list(),
    '#default_value' => isset($yandex['fns_paymentSubjectType']) ? $yandex['fns_paymentSubjectType'] : 'no',
  ];
  $form['fns']['fns_excise'] = [
    '#type' => 'textfield',
    '#title' => t('Excise'),
    '#default_value' => isset($yandex['fns_excise']) ? $yandex['fns_excise'] : '',
  ];
  $form['fns']['fns_productCode'] = [
    '#type' => 'textfield',
    '#title' => t('Product code'),
    '#default_value' => isset($yandex['fns_productCode']) ? $yandex['fns_productCode'] : '',
  ];
  $form['fns']['fns_markQuantity_1'] = [
    '#type' => 'textfield',
    '#title' => t('Fractional quantity of marked goods (numerator)'),
    '#default_value' => isset($yandex['fns_markQuantity_1']) ? $yandex['fns_markQuantity_1'] : '',
  ];
  $form['fns']['fns_markQuantity_2'] = [
    '#type' => 'textfield',
    '#title' => t('Fractional quantity of marked goods (denominator)'),
    '#default_value' => isset($yandex['fns_markQuantity_2']) ? $yandex['fns_markQuantity_2'] : '',
  ];
  $form['fns']['fns_text_1'] = [
    '#markup' => '<h4>'.t('Vendor Data').'</h4>',
  ];
  $form['fns']['fns_supplier_name'] = [
    '#type' => 'textfield',
    '#title' => t('Supplier name'),
    '#default_value' => isset($yandex['fns_supplier_name']) ? $yandex['fns_supplier_name'] : '',
  ];
  $form['fns']['fns_supplier_inn'] = [
    '#type' => 'textfield',
    '#title' => t('TIN of the supplier'),
    '#default_value' => isset($yandex['fns_supplier_inn']) ? $yandex['fns_supplier_inn'] : '',
  ];
  $form['fns']['fns_supplier_phones'] = [
    '#type' => 'textfield',
    '#title' => t('Vendor phone numbers. Comma separated in the format +79995554444'),
    '#default_value' => isset($yandex['fns_supplier_phones']) ? $yandex['fns_supplier_phones'] : '',
  ];
  $form['fns']['fns_text_2'] = [
    '#markup' => '<h4>'.t('Agent Data').'</h4>',
  ];
  $form['fns']['fns_agent_type'] = [
    '#type' => 'select',
    '#title' => t('Indicator of the agent on the subject of settlement'),
    '#options' => fns_agent_type_list(),
    '#default_value' => isset($yandex['fns_agent_type']) ? $yandex['fns_agent_type'] : 'no',
  ];
  $form['fns']['fns_agent_operation'] = [
    '#type' => 'textfield',
    '#title' => t('Payment agent operation'),
    '#default_value' => isset($yandex['fns_agent_operation']) ? $yandex['fns_agent_operation'] : '',
  ];
  $form['fns']['fns_agent_phones'] = [
    '#type' => 'textfield',
    '#title' => t('Payment agent phone numbers. Comma separated in the format +79995554444'),
    '#default_value' => isset($yandex['fns_agent_phones']) ? $yandex['fns_agent_phones'] : '',
  ];
  $form['fns']['fns_agent_transferOperator_name'] = [
    '#type' => 'textfield',
    '#title' => t('Transfer operator name'),
    '#default_value' => isset($yandex['fns_agent_transferOperator_name']) ? $yandex['fns_agent_transferOperator_name'] : '',
  ];
  $form['fns']['fns_agent_transferOperator_address'] = [
    '#type' => 'textfield',
    '#title' => t('Transfer operator address'),
    '#default_value' => isset($yandex['fns_agent_transferOperator_address']) ? $yandex['fns_agent_transferOperator_address'] : '',
  ];
  $form['fns']['fns_agent_transferOperator_inn'] = [
    '#type' => 'textfield',
    '#title' => t('TIN of transfer operator'),
    '#default_value' => isset($yandex['fns_agent_transferOperator_inn']) ? $yandex['fns_agent_transferOperator_inn'] : '',
  ];
  $form['fns']['fns_agent_transferOperator_phones'] = [
    '#type' => 'textfield',
    '#title' => t('Transfer operator phone numbers. Comma separated in the format +79995554444'),
    '#default_value' => isset($yandex['fns_agent_transferOperator_phones']) ? $yandex['fns_agent_transferOperator_phones'] : '',
  ];
  $form['fns']['fns_agent_paymentsOperator_phones'] = [
    '#type' => 'textfield',
    '#title' => t('Phones of the payment acceptance operator. Comma separated in the format +79995554444'),
    '#default_value' => isset($yandex['fns_agent_paymentsOperator_phones']) ? $yandex['fns_agent_paymentsOperator_phones'] : '',
  ];  

  $form['submit'] = [
    '#type' => 'submit',
    '#name' => 'submit',
    '#value' => t('Save'), 
  ];

  $form['#attached']['css'] = [
    drupal_get_path('module', 'commerce_yandexpay') . '/css/settings.css',
  ];

	return $form;	
}

function yandex_settings_form_validate($form, &$form_state) {
 
}

function yandex_settings_form_submit($form, &$form_state){

  $form_state['values']['btn_class'] = str_replace(['.', '#'], '', $form_state['values']['btn_class']);
  $form_state['values']['api_url'] = 'https://sandbox.pay.yandex.ru';
  
  if($form_state['values']['working_mode'] == 'prod'){
    $form_state['values']['api_url'] = 'https://pay.yandex.ru';
  }

  variable_set('yandex_settings', $form_state['values']);

	return $form;
}
