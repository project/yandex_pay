<div class="com-yandexpay">
  <fieldset class="form-wrapper">
    <legend>
      <span class="fieldset-legend"><?=t("Order list")?></span>
    </legend>  
    
    <div class="fieldset-wrapper">
      <div class="form-item form-type-select">
        <select id="edit-order-list" name="order_list" class="form-select">
          <option value="0" >-</option>
          <?php foreach($orders as $order):?>
            <?php $oid = explode('_', $order); ?>
            <option value="<?=$order?>" >№ <?=$oid[0]?></option>          
          <?php endforeach; ?>        
        </select>  
      </div>
    </div>
  </fieldset>  
  
  <fieldset class="form-wrapper">
    <legend>
      <span class="fieldset-legend"><?=t("Order details")?></span>
    </legend> 
    
    <div class="fieldset-wrapper">
      <div class="items details"><!-- JS content --></div>      

      <div class="action">      
        <div class="btn yp-btn-refund"><?=t("Apply")?></div>                
      </div>
      <div class="info yp-inf-refund"><!-- JS content --></div>        
      
    </div>
  </fieldset> 
  
  <div class="item tmp">
    <div class="yp-name"></div>
    <div class="dtl">

      <div class="yp-qty">
        <span><?=t("Qty")?></span>
        <input type="text" value="0">
      </div> 
      
      <div class="yp-unitPrice">
        <span><?=t("Total unit price")?></span>
        <input type="text" value="0">
      </div> 
      
      <div class="yp-discountedUnitPrice">
        <span><?=t("Unit price including discount")?></span>
        <input type="text" value="0">
      </div>
      
      <div class="yp-subtotal">
        <span><?=t("Total price per item excluding discounts")?></span>
        <input type="text" value="0">
      </div>
      
      <div class="yp-total">
        <span><?=t("Total price per item with all discounts")?></span>
        <input type="text" value="0">
      </div>
      
      <div class="yp-shipping">
        <span><?=t("Cost of delivery")?></span>
        <input type="text" value="0">
      </div>
      
    </div>
  </div> 
</div>

<script>
  var txtDelivery = '<?=t("Delivery")?>'; 
  var refundSuccessfully = '<?=t("Operation completed successfully")?>'; 
</script>